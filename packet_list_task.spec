%global __brp_check_rpaths %{nil}
%define __arch_install_post %{nil}
%define __find_provides %{nil}
%define __find_requires %{nil}

Autoprov: 0
Autoreq: 0

Name:           packet-list-task
Version:        1.0
Release:        1
Summary:        Filter packages from 2 branches
Group:          Application/Program
BuildArch:      x86_64
BuildRoot:      %{_tmppath}/test-root

License: GPLv3
Source0: %{name}.tar.gz

%description
App for package filtration

%prep
%{__rm} -rf %{name}-%{version}
%{__mkdir} -p %{name}-%{version}
%{__tar} -xzvf %{SOURCE0} -C %{_builddir}/%{name}-%{version} --strip-components 1

%build
cd %{name}-%{version}; mkdir build; cd build
cmake .. -DCMAKE_INSTALL_PREFIX:PATH="%{buildroot}/usr/local"
cmake ..; cmake --build .

%install
cd %{name}-%{version}/build
mkdir -p $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/usr/local/source
mkdir -p $RPM_BUILD_ROOT/usr/local/library
cmake --install .
%set_verify_elf_method skip

%files
%defattr(-,root,root)
/usr/local/lib/librestapi_client.so
/usr/local/bin/packet_list_task

%post
patchelf --set-rpath /usr/local/lib /usr/local/bin/packet_list_task

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf $RPM_BUILD_DIR/*