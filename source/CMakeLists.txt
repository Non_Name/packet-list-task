cmake_minimum_required(VERSION 3.10)

project(packet_list_task VERSION 1.0.0 LANGUAGES CXX)

add_executable(${PROJECT_NAME} main.cpp Application.cpp)
target_link_libraries(
    ${PROJECT_NAME}
    PRIVATE
    ${Boost_LIBRARIES}
    restapi_client
)
