#include "Application.hpp"
#include <ApiClient.hpp>
#include <CompFilter.hpp>

/////CLI Implementation/////

Application::Application(int argc, char *argv[]) 
    : m_arg_size(argc), m_arguments(argv)
{
}

void Application::exec()
{
    options_description description("Options");
    description.add_options()("help,h", "Show help")(
        "branch,b", boost::program_options::value<std::vector<std::string>>(),//not found more elegant solutions(std::vector) =(
        "Select 2 branches for lists of binary packets")
        ("compare,c", "Compare version-release")
        ("first,f", "Get unique packages from first branch")
        ("second,s", "Get unique packages from second branch");
    variables_map variables;
    try
    {
        std::vector<std::string> branches;
        prog_options::store(prog_options::parse_command_line(m_arg_size, m_arguments, description), variables);
        prog_options::notify(variables);
        if(variables.count("help") || m_arg_size == 1)
        {
            std::cout << description;
            return;
        }
        if(variables.count("branch"))
        {
            std::vector<std::string> branches = variables["branch"].as<std::vector<std::string>>();
            if(branches.size() < 2 || branches.size() > 2)
            {
                throw std::logic_error("Number of branches must be 2");
            }
            processArgs(variables, branches);
        }
        else
        {
            throw std::logic_error("Branches not entered");
        }
    }
    catch(const std::exception & exception)
    {
        std::cerr << "ERROR: " << exception.what() << "\n";
    }
}

void Application::processArgs(const variables_map & var_map, const std::vector<std::string> & branches)
{
    NetworkLayer::iocontext iocontext = std::make_shared<boost::asio::io_context>();
    NetworkLayer::ApiClient client(iocontext);
    bool status = client.connect();
    if(status)
    {
        client.createRequest(branches[0]);
        auto first_response = client.getResponse();
        client.createRequest(branches[1]);
        auto second_response = client.getResponse();
        if(first_response.status == NetworkLayer::http::status::ok && second_response.status == NetworkLayer::http::status::ok)
        {
            auto first_branch = CompareLayer::CompFilter::processBranches(first_response.body);
            auto second_branch = CompareLayer::CompFilter::processBranches(second_response.body);
            if(var_map.count("second"))
            {
                CompareLayer::CompFilter::comparePackets(second_branch, first_branch, "second_branch.json");
            }
            if(var_map.count("first"))
            {
                CompareLayer::CompFilter::comparePackets(first_branch, second_branch, "first_branch.json");
            }
            if(var_map.count("compare"))
            {
                CompareLayer::CompFilter::comparePackets(first_branch, second_branch, "versions-releases.json", true);
            }
        }
        else
        {
            throw std::logic_error
            (
                "First response " + first_response.reason + "\n"
                "Second response " + second_response.reason
            );
        }
    }
}
