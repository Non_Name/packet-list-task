# packet-list-task

## Technology Stack

- C++
- Boost asio/beast
- Boost program-options
- CMake
- OpenSSL

## Libs and utils for app running

- Boost
- CMake 
- OpenSSL 

## Build & Run

```
mkdir build
cd build
cmake ..
cmake --build .
./source/packet_list_task
```
## Intallation Guide

### Preparation

- Install rpmdevtools (or use `mkdir -p ~/RPM/{SOURCES,BUILD,SPECS,RPMS,SRPMS}`)
- Install rpm-build
- Install patchelf
- Run command `rpmdev-setuptree`

Archive local repository and move:
```
cd packet-list-task
git archive --format=tar.gz -o packet-list-task.tar.gz HEAD
mv packet-list-task.tar.gz ~/RPM/SOURCES
```

### RPM packet creation

Run rpmbuild:
```
rpmbuild -ba packet_list_task.spec
```

### Installation

- Move to the `~/RPM/RPMS`
- Use command `rpm -i packet-list-task-1.0-1.x86_64.rpm` (with root permissions)
- Or just click on the file and enter the password
- Run `packet_list_task` to check

IMPORTANT: Install all recommended libs and utils for correct installation

## CLI
| Options         | Description                            |
|-----------------|----------------------------------------|
| `-h, --help`    |   Show help                            |
| `-f, --first`   |   Get unique packages(first branch)    |
| `-s, --second`  |   Get unique packages(second branch)   |
| `-c, --compare` |   Get packages with highest version    |
| `-b, --branch`  |   Set branch names(must be used twice) |

IMPORTANT: flags -c, -s, -f, are necessary too (or one of them), if you want get JSON files

### CLI Example
```
./source/packet_list_task -b sisyphus -b p9 -f -c
```

## Output

In the example above we get next files(in `build` folder):
- first_branch.json
- version-releases.json

## Platform

Ubuntu Linux 22.04 (compilation)
Alt Linux K10 (compilation/installation)
Fedora OS (compilation/installation)