#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include <boost/program_options.hpp>
#include <iostream>
#include <vector>

namespace prog_options = boost::program_options;
using variables_map = prog_options::variables_map;
using options_description = prog_options::options_description;
class Application
{
public:
    Application(int argc, char *argv[]);
    Application() = delete;
    void exec();

private:
    void processArgs(const variables_map & var_map, const std::vector<std::string> & branches);
    char **m_arguments;
    int m_arg_size{0};
    std::vector<std::string> m_branches;
};

#endif // APPLICATION_HPP