#include "CompFilter.hpp"

namespace CompareLayer
{
//Set JSON's data into multimap
BinaryPacket CompFilter::processBranches(const std::string & branch)
{
    std::stringstream stream_branch{branch};
    BinaryPacket binary_packet;
    ptree p_tree;
    property_tree::read_json(stream_branch, p_tree);
    ptree packages = p_tree.get_child("packages");
    for(const auto & i : packages)
    {
        PacketParams params;
        std::string name = i.second.get<std::string>("name");
        params.arch = i.second.get<std::string>("arch");
        params.vers_rel.release = i.second.get<std::string>("release");
        params.vers_rel.version = i.second.get<std::string>("version");
        binary_packet.insert(std::pair<std::string, PacketParams>(name, params));
    }
    return binary_packet;
}

void CompFilter::comparePackets(const BinaryPacket & first_branch, const BinaryPacket & second_branch, const std::string & filename, bool comp_vers)
{
    ptree p_tree;
    ptree package_dicts;    
    for(const auto & i : first_branch)
    {
        auto result = second_branch.equal_range(i.first);//get list of values with equal key
        auto eq_status = isEqual(i, result);
        if(eq_status.status && comp_vers)
        {
            std::vector<size_t> first_version = removeDots(i.second.vers_rel.version); //for version comparison
            std::vector<size_t> second_version = removeDots(eq_status.vers_rel.version);
            size_t first_release = getRelease(i.second.vers_rel.release);//get release number(before symbols)
            size_t second_release = getRelease(eq_status.vers_rel.release);
            if(compareVersions(first_version, second_version) && first_release >= second_release)
            {
                putParams(package_dicts, i);//generate dictionary
            }
        }
        else if(!comp_vers && !eq_status.status)
        {
            putParams(package_dicts, i);
        }
    }
    p_tree.add_child("packages", package_dicts);//put into array
    property_tree::write_json(filename, p_tree);//generate JSON file
}

std::vector<size_t> CompFilter::removeDots(const std::string & version)
{
    size_t vers_num{0};
    std::vector<size_t> result;
    for(auto i = version.begin(); i != version.end(); ++i)
    {
        if(*i != '.' && !isalpha(*i))//ignore symbols and dots
        {
            vers_num = (vers_num * 10) + (*i - ASCII_OFFSET);
        }
        if(*i == '.' || i + 1 == version.end())
        {
            result.push_back(vers_num);
            vers_num = 0;
        }
    }
    return result;
}

bool CompFilter::compareVersions(const std::vector<size_t> & first_vers, const std::vector<size_t> & second_vers)
{
    auto first_it = first_vers.begin();
    auto second_it = second_vers.begin();
    while(first_it != first_vers.end() || second_it != second_vers.end())
    {
        if(*first_it > *second_it)
        {
            return true;
        }
        first_it++;
        second_it++;
    }
    return false;
}

size_t CompFilter::getRelease(const std::string & release)
{
    size_t release_num{0};
    for(auto i = release.begin() + 3; i != release.end(); ++i)
    {
        if(isdigit(*i))
        {
            release_num = (release_num * 10) + (*i - ASCII_OFFSET);
        }
        else
        {
            break;
        }        
    }
    return release_num;
}

EqualStatus CompFilter::isEqual(const CompParams & first_branch, PacketIter & second_branch)
{
    EqualStatus eq_status;
    if(second_branch.first != second_branch.second)
    {
        for(auto j = second_branch.first; j != second_branch.second; ++j)
        {
            if(first_branch.second.arch == j->second.arch)//compare arches
            {
                eq_status.status = true;
                eq_status.vers_rel.release = j->second.vers_rel.release;//put necessary information for version-release processing
                eq_status.vers_rel.version = j->second.vers_rel.version;
                break;
            }               
        }
    }
    return eq_status;
}

void CompFilter::putParams(ptree & package_dicts, const CompParams & first_branch)
{
    ptree params;
    params.put("name", first_branch.first);
    params.put("arch", first_branch.second.arch);
    params.put("version", first_branch.second.vers_rel.version);
    params.put("release", first_branch.second.vers_rel.release);
    package_dicts.push_back(std::make_pair("", params));
}

}