#include "ApiClient.hpp"

namespace NetworkLayer
{
ApiClient::ApiClient(const iocontext & iocontext) 
    : m_iocontext(iocontext), m_context(ssl::context::method::sslv23_client),
      m_socket(*m_iocontext, m_context)
{
    m_context.set_default_verify_paths();
    m_socket.set_verify_mode(ssl::verify_peer);
}
//SSL verification and connection
bool ApiClient::connect()
{
    try
    {
        m_socket.set_verify_callback(boost::bind(&ApiClient::verify, this, boost::placeholders::_1, boost::placeholders::_2));
        tcp::resolver resolver(*m_iocontext);
        auto endpoints = resolver.resolve(m_address.host, m_address.port);
        asio::connect(m_socket.lowest_layer(), endpoints);
        m_socket.handshake(ssl::stream_base::handshake_type::client); 
    }
    catch(const std::exception & exception)
    {
        std::cerr << "ERROR: " << exception.what() << "\n";
        return false; 
    }
    return true;
}

bool ApiClient::verify(bool preverified, boost::asio::ssl::verify_context& ctx)//verification
{
    return true;
}

void ApiClient::createRequest(const std::string & branch)
{
    std::string url = PROTOCOL + m_address.host + PATH + branch;
    http::request<http::string_body> request{http::verb::get, url, HTTP_VERSION};
    request.set(http::field::host, m_address.host);
    request.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
    request.set(http::field::content_type, "application/json");
    http::write(m_socket, request);
}
//Get packages
Response ApiClient::getResponse()
{
    Response result;
    http::response_parser<http::string_body> response;
    boost::beast::flat_buffer buffer;
    response.body_limit((std::numeric_limits<std::uint64_t>::max)());
    http::read(m_socket, buffer, response);
    auto message = response.release();
    result.status = message.result();
    result.reason = message.reason().data();
    result.body = message.body().data();
    return result;
}
//Close connection
ApiClient::~ApiClient()
{
    m_socket.lowest_layer().shutdown(tcp::socket::shutdown_both);
}

}