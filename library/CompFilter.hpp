#ifndef COMP_FILTER_HPP
#define COMP_FILTER_HPP

#include <algorithm>
#include <iostream>
#include <map>
#include <vector>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/tokenizer.hpp>

namespace CompareLayer
{

struct VersionRelease
{
    std::string version;
    std::string release;    
};

struct PacketParams
{
    std::string arch;
    VersionRelease vers_rel;
};

struct EqualStatus
{
    bool status{false};
    VersionRelease vers_rel;
};

namespace property_tree = boost::property_tree;
using BinaryPacket = std::multimap<std::string, PacketParams>;
using ptree = boost::property_tree::ptree;
using CompParams = std::pair<std::string, PacketParams>;
using PacketIter = std::pair<BinaryPacket::const_iterator, BinaryPacket::const_iterator>;
using tokenizer = boost::tokenizer<boost::char_separator<char>>;

constexpr size_t ASCII_OFFSET = 48;

class CompFilter
{
public:
    static BinaryPacket processBranches(const std::string & branch);
    static void comparePackets(const BinaryPacket & first_branch, const BinaryPacket & second_branch, const std::string & filename, bool comp_vers = false);

private:
    static EqualStatus isEqual(const CompParams & first_branch, PacketIter & second_branch);
    static void putParams(ptree & package_dicts, const CompParams & first_branch); 
    static std::vector<size_t> removeDots(const std::string & version);
    static size_t getRelease(const std::string & release);
    static bool compareVersions(const std::vector<size_t> & first_vers, const std::vector<size_t> & second_vers);
};

}

#endif // COMP_FILTER_HPP
