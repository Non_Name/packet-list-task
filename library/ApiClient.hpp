#ifndef API_CLIENT_HPP
#define API_CLIENT_HPP

#include <boost/asio.hpp>
#include <boost/beast.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <iostream>
#include <memory>
#include <boost/bind/bind.hpp>

namespace NetworkLayer
{
namespace asio = boost::asio;
namespace ssl = asio::ssl;
namespace beast = boost::beast;
namespace http = beast::http;
using tcp = asio::ip::tcp;
using udp = asio::ip::udp;
using iocontext = std::shared_ptr<asio::io_context>;

const std::string PATH = "/api/export/branch_binary_packages/";
constexpr size_t HTTP_VERSION = 11;
const std::string PROTOCOL = "https://";

struct IP
{
    std::string host;
    std::string port;
};

struct Response
{
    http::status status;
    std::string reason;
    std::string body;
};

class ApiClient
{
public:
    explicit ApiClient(const iocontext & iocontext);
    ApiClient() = delete;
    ~ApiClient();
    bool verify(bool preverified, boost::asio::ssl::verify_context& ctx);
    bool connect();
    void createRequest(const std::string & branch);
    Response getResponse();
    
private:
    iocontext m_iocontext;
    IP m_address{"rdb.altlinux.org", "443"};
    ssl::context m_context;
    ssl::stream<tcp::socket> m_socket;
};

}

#endif // API_CLIENT_HPP